via the script
-
1. make the script executable
```
chmod +x uwf-setup
```
2. run the script
```
./uwf-setup
```

or do it manualy
-

Firewall Configuration on Arch Linux

This guide addresses the issue of firewall settings not persisting across reboots on Arch Linux. The steps below ensure that your firewall rules remain active and effective after a system restart.

Table of Contents

- Prerequisites
- Using UFW
  - Install UFW
  - Configure UFW
  - Ensure UFW Starts on Boot
- NetworkManager Integration
- Troubleshooting

Prerequisites

Ensure you have administrative (sudo) privileges on your Arch Linux system.

Using UFW

Install UFW

First, install UFW if it is not already installed:

```
sudo pacman -S ufw``
```

Configure UFW

1. Enable UFW and start the service:

```
sudo systemctl enable ufw
sudo systemctl start ufw

```

2. Add your firewall rules. For example:
```
sudo ufw allow ssh
sudo ufw allow 80/tcp
sudo ufw allow 443/tcp
sudo ufw default deny incoming
sudo ufw enable
```

3. Reload UFW to apply the changes:

sudo ufw reload

Ensure UFW Starts on Boot

Check the status of the UFW service to confirm it is active and enabled:

sudo systemctl status ufw

If UFW is not running, start it:
```
sudo systemctl start ufw
```

NetworkManager Integration

If you are using NetworkManager, it may override your firewall settings on network changes. To ensure UFW rules are reapplied, create a dispatcher script:

1. Create the script:
```
sudo nano /etc/NetworkManager/dispatcher.d/50-ufw-restore
```
2. Add the following content to the script:
```
#!/bin/bash
/usr/sbin/ufw reload
```

3. Make the script executable:
```
sudo chmod +x /etc/NetworkManager/dispatcher.d/50-ufw-restore
```
Troubleshooting

If you continue to experience issues, follow these steps:

1. Verify UFW Service Status:

Ensure the UFW service is enabled and active:
```
sudo systemctl status ufw
```
2. Check for Conflicting Services:

Ensure no other firewall services (e.g., firewalld) are conflicting with UFW:

```
sudo systemctl list-units --type=service | grep firewall
```
3. Review Logs:

Check system logs for any errors related to firewall rule loading:

```
sudo journalctl -xe
```

Following these steps should ensure that your firewall settings remain persistent and effective across reboots, preventing any connectivity issues caused by firewall misconfigurations.
